/* eslint-disable quotes */
/* eslint-disable no-unused-vars */
/* eslint-disable indent */
'use strict';

const { finalizeSession } = require("pg/lib/sasl");

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     await queryInterface.createTable('users', { 
      id: {
       type: Sequelize.INTEGER,
       autoIncrement: true,
       primaryKey: true,
       allowNull: false
     },
     user_id:{
       type: Sequelize.UUID,
       allowNull: false
     },
     firstname:{
       type: Sequelize.STRING,
       allowNull: false
     },
     lastname: {
       type: Sequelize.STRING,
       allowNull: false
     },
     email: {
       type: Sequelize.STRING,
       allowNull: false,
       unique: true
     },
     password: {
       type: Sequelize.STRING,
       allowNull: false
     },
     group_id: {
       type: Sequelize.INTEGER,
       allowNull: false
     },
     passport_path: {
       type: Sequelize.STRING,
       allowNull: true
     },
     created_by:{
       type: Sequelize.INTEGER,
       allowNull: false
     },
     created_at:{
       type: Sequelize.DATE,
       allowNull: false
     },
     updated_at:{
       type: Sequelize.DATE,
       allowNull: true
     }
    });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
     await queryInterface.dropTable('users');
  }
};
