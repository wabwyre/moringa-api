/* eslint-disable quotes */
/* eslint-disable semi */
/* eslint-disable no-async-promise-executor */
/* eslint-disable indent */
/* eslint-disable no-unused-vars */
'use strict';
const bcrypt = require('bcrypt');
const uuid = require('uuid').v4;
let user_id = uuid();
const moment = require('moment');
const { exitOnError } = require('winston');
 console.log(user_id);
const makePassword = (pw) => {
  return new Promise(async rs => {
    let salt, hash;
    salt = await bcrypt.genSalt(10);
    hash = await bcrypt.hash(pw, salt);
    return rs(hash);
  })
}

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     let password = await makePassword("secret");
    await queryInterface.bulkInsert('users', [{
        user_id: user_id,
         firstname: 'System',
         lastname: 'Admin',
         email: 'system.admin@email.com',
         password: password,
         group_id: '1',
         created_by: '1',
         created_at: moment().format('YYYY-MM-DD')
       }], {});

  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('users', null, {});
  }
};
