/* eslint-disable no-unused-vars */
'use strict';

const moment = require('moment');

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('admin_roles', [{
      name: 'DASHBOARD_MENU',
      code: 'DASHBOARD_MENU',
      created_at: moment().format('YYYY-MM-DD')
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('admin_roles', null, {});
  }
};
