// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const adminGroupRoles = sequelizeClient.define('admin_group_roles', {
    'id': {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      comment: 'null',
      autoIncrement: true
    },
    'group_id': {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      comment: 'null'
    },
    'role_id': {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      comment: 'null'
    },
    'created_at': {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.fn('current_timestamp'),
      comment: 'null'
    },
    'updated_at': {
      type: DataTypes.DATE,
      allowNull: true,
      comment: 'null'
    }

  }, {
    timestamps: false, tableName: 'admin_group_roles'
  });

  // eslint-disable-next-line no-unused-vars
  adminGroupRoles.associate = function (models) {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    adminGroupRoles.belongsTo(models.admin_roles, {
      as: 'role',
      foreignKey: 'role_id',
      //adjacent key in admin_roles table
      targetKey: 'id'
    });

    adminGroupRoles.belongsTo(models.admin_groups, {
      as: 'group',
      foreignKey: 'group_id',
      //adjacent key in admin_roles table
      targetKey: 'id'
    });
  };

  return adminGroupRoles;
};
