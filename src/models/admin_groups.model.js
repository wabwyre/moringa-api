// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const adminGroups = sequelizeClient.define('admin_groups', {
    'id': {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      comment: 'null',
      autoIncrement: true
    },
    'name': {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: 'null',
      unique: true
    },
    'created_at': {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.fn('current_timestamp'),
      comment: 'null'
    },
    'updated_at': {
      type: DataTypes.DATE,
      allowNull: true,
      comment: 'null'
    }
  }, {
    timestamps: false, tableName: 'admin_groups'
  });

  // eslint-disable-next-line no-unused-vars
  adminGroups.associate = function (models) {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
  };

  return adminGroups;
};
