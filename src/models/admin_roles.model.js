// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const adminRoles = sequelizeClient.define('admin_roles', {
    'id': {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      comment: 'null',
      autoIncrement: true
    },
    'code': {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: 'null',
      unique: true
    },
    'name': {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: 'null'
    },
    'created_at': {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.fn('current_timestamp'),
      comment: 'null'
    },
    'updated_at': {
      type: DataTypes.DATE,
      allowNull: true,
      comment: 'null'
    }
  }, {
    timestamps: false, tableName: 'admin_roles'
  });

  // eslint-disable-next-line no-unused-vars
  adminRoles.associate = function (models) {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
  };

  return adminRoles;
};
