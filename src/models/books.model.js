/* eslint-disable quotes */
// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const books = sequelizeClient.define('books', {
    'id': {
      type: DataTypes.INTEGER().UNSIGNED,
      allowNull: false,
      primaryKey: true,
      comment: "null",
      autoIncrement: true
    },
    'name': {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: "null"
    },
    'isbn': {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: "null"
    },
    'authors': {
      type: DataTypes.JSON,
      allowNull: false,
      comment: "null"
    },
    'pages': {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      comment: "null"
    },
    'publisher': {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: "null"
    },
    'country': {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: "null"
    },
    'media_type': {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: "null"
    },
    'created_at': {
      type: DataTypes.DATE,
      allowNull: false,
      comment: "null"
    },
    'created_by': {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      comment: "null"
    }, 
    'released': {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.fn('current_timestamp'),
      comment: "null"
    },
    'updated_at': {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.fn('current_timestamp'),
      comment: "null"
    }
  }, {
    timestamps: false, tableName: 'books'
  });

  // eslint-disable-next-line no-unused-vars
  books.associate = function (models) {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
    books.belongsTo(models.users, {
      as: 'users',
      foreignKey: 'created_by',
      //adjacent key in users table
      targetKey: 'id'
    });
   
  };

  return books;
};
