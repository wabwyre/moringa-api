/* eslint-disable quotes */
// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const characters = sequelizeClient.define('characters', {
    'id': {
      type: DataTypes.INTEGER().UNSIGNED,
      allowNull: false,
      primaryKey: true,
      comment: "null",
      autoIncrement: true
    },
    'name': {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: "null"
    },
    'gender': {
      type: DataTypes.ENUM('male', 'female'),
      allowNull: false,
      comment: "null"
    },
    'culture': {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: "null"
    },
    'age': {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: "null"
    },
    'played_by': {
      type: DataTypes.JSON,
      allowNull: false,
      comment: "null"
    },
    'created_by': {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      comment: "null"
    },
    'created_at': {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.fn('current_timestamp'),
      comment: "null"
    },
    'updated_at': {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.fn('current_timestamp'),
      comment: "null"
    }
  }, {
    timestamps: false, tableName: 'characters'
  });

  // eslint-disable-next-line no-unused-vars
  characters.associate = function (models) {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
    characters.belongsTo(models.users, {
      as: 'users',
      foreignKey: 'created_by',
      //adjacent key in users table
      targetKey: 'id'
    });
  };

  return characters;
};
