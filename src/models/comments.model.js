/* eslint-disable quotes */
// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const comments = sequelizeClient.define('comments', {
    'id': {
      type: DataTypes.INTEGER().UNSIGNED,
      allowNull: false,
      primaryKey: true,
      comment: "null",
      autoIncrement: true
    },
    'comment': {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: "null"
    },
    'ip_address': {
      type: DataTypes.INET,
      allowNull: false,
      comment: "null"
    },
    'book_id': {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      comment: "null"
    }, 
    'created_by': {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      comment: "null"
    },
    'created_at': {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.fn('current_timestamp'),
      comment: "null"
    },
    'updated_at': {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.fn('current_timestamp'),
      comment: "null"
    }
  }, {
    timestamps: false, tableName: 'comments'
  });

  // eslint-disable-next-line no-unused-vars
  comments.associate = function (models) {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
    comments.belongsTo(models.users, {
      as: 'users',
      foreignKey: 'created_by',
      //adjacent key in users table
      targetKey: 'id'
    });

    comments.belongsTo(models.books, {
      as: 'books',
      foreignKey: 'book_id',
      //adjacent key in books table
      targetKey: 'id'
    });
        
  };

  return comments;
};
