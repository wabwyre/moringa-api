/* eslint-disable quotes */
// See https://sequelize.org/master/manual/model-basics.html
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const users = sequelizeClient.define('users', {
    'id': {
      type: DataTypes.INTEGER().UNSIGNED,
      allowNull: false,
      primaryKey: true,
      comment: "null",
      autoIncrement: true
    },
    'user_id': {
      type: DataTypes.UUID(),
      allowNull: false,
      comment: "null"
    },
    'firstname': {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: "null"
    },
    'lastname': {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: "null"
    },
    'email': {
      type: DataTypes.STRING(255),
      allowNull: false,
      unique: true
    },
    'password': {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    'passport_path': {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: "null"
    },
    'group_id': {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      comment: "null"
    },
    'created_by': {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      comment: "null"
    },
    'created_at': {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.fn('current_timestamp'),
      comment: "null"
    },
    'updated_at': {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.fn('current_timestamp'),
      comment: "null"
    }
  }, {
    timestamps: false, tableName: 'users'
  });

  // eslint-disable-next-line no-unused-vars
  users.associate = function (models) {
    // Define associations here
    // See https://sequelize.org/master/manual/assocs.html
    users.belongsTo(models.admin_groups, {
      as: 'roles',
      foreignKey: 'group_id',
      //adjacent key in admin_groups table
      targetKey: 'id'
    });
  };

  return users;
};
