const { authenticate } = require('@feathersjs/authentication').hooks;
const addAssociations = require('./../../hooks/add-associations');

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [addAssociations({
      models: [
        {
          model: 'admin-roles',
          as: 'role',
          attributes: ['id', 'code','name'],
        },
        {
          model: 'admin-groups',
          as: 'group',
          attributes: ['id', 'name'],
        }
      ]
    }) 
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
