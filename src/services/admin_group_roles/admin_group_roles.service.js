// Initializes the `admin_group_roles` service on path `/admin-group-roles`
const { AdminGroupRoles } = require('./admin_group_roles.class');
const createModel = require('../../models/admin_group_roles.model');
const hooks = require('./admin_group_roles.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/admin-group-roles', new AdminGroupRoles(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('admin-group-roles');

  service.hooks(hooks);
};
