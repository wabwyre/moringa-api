// Initializes the `admin_groups` service on path `/admin-groups`
const { AdminGroups } = require('./admin_groups.class');
const createModel = require('../../models/admin_groups.model');
const hooks = require('./admin_groups.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/admin-groups', new AdminGroups(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('admin-groups');

  service.hooks(hooks);
};
