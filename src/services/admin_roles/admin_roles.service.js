// Initializes the `admin_roles` service on path `/admin-roles`
const { AdminRoles } = require('./admin_roles.class');
const createModel = require('../../models/admin_roles.model');
const hooks = require('./admin_roles.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/admin-roles', new AdminRoles(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('admin-roles');

  service.hooks(hooks);
};
