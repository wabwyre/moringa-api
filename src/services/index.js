const users = require('./users/users.service.js');
const utils = require('./utils/utils.service.js');
const books = require('./books/books.service.js');
const comments = require('./comments/comments.service.js');
const characters = require('./characters/characters.service.js');
const adminGroups = require('./admin_groups/admin_groups.service.js');
const adminRoles = require('./admin_roles/admin_roles.service.js');
const adminGroupRoles = require('./admin_group_roles/admin_group_roles.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(utils);
  app.configure(books);
  app.configure(comments);
  app.configure(characters);
  app.configure(adminGroups);
  app.configure(adminRoles);
  app.configure(adminGroupRoles);
};
