/* eslint-disable indent */
/* eslint-disable no-unused-vars */
const {
    NotFound,
    BadRequest
  } = require('@feathersjs/errors');
  
  const errors = require('@feathersjs/errors');
  
  const notImplemented = method => {
    throw new errors.NotImplemented(`${method} is not implemented on this service`);
  };
  
  class Service {
    constructor(options) {
      this.options = options || {};
    }
  
    async create(data, params) {
      notImplemented('create');
    }
  
    async get(params) {
      notImplemented('get');
    }
  
    async find() {
      return Promise.resolve({
        result: 'Token is valid',
      });
    }
  
    async update() {
      notImplemented('update');
    }
  
    async patch() {
      notImplemented('patch');
    }
  
    async remove() {
      notImplemented('remove');
    }
  }
  
  module.exports = function () {
    return new Service();
  };
  
  module.exports.Service = Service;
  