/* eslint-disable no-unused-vars */
/* eslint-disable quotes */
const { NotFound, BadRequest } = require("@feathersjs/errors");

const Promise = require("bluebird");
const errors = require("@feathersjs/errors");
const moment = require("moment");
const request = require("request");

const notImplemented = (method) => {
  throw new errors.NotImplemented(
    `${method} is not implemented on this service`
  );
};

class Service {
  constructor(options) {
    this.options = options || {};
  }

  async create(dat, params) {
    const sequelize = this.options.sequelize;
    const context = this;

    //   console.log(data);
    //console.log(params);

    const { payload } = params.authentication;



    let createdBy = payload.id;
    let data = dat[0].all;
   

    console.log(createdBy);

   
    if (
      !data.name ||
          !data.isbn ||
          !data.pages
    ) throw new BadRequest('Please provide all the information needed to create a book');

    await params.transaction.then((t) => {
      
      return sequelize.models.books.create({
        raw: true,
        name: data.name,
        isbn: data.isbn,
        authors: data.authors,
        pages: data.pages,
        publisher: data.publisher,
        country: data.country,
        media_type: data.media_type,
        released: data.released,
        created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
        created_by: createdBy
      }, {
        transaction: t
      });
    });

   
    return Promise.resolve({
      result: "Book record has been added successfully!",
      book: data.name,
    });
  }

  async get() {
    notImplemented("get");
  }

  async find() {
    notImplemented("find");
  }

  async update() {
    notImplemented("update");
  }

  async patch() {
    notImplemented("patch");
  }

  async remove() {
    notImplemented("remove");
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
