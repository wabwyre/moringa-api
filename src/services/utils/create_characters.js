/* eslint-disable no-unused-vars */
/* eslint-disable quotes */
const { NotFound, BadRequest } = require("@feathersjs/errors");

const Promise = require("bluebird");
const errors = require("@feathersjs/errors");
const moment = require("moment");
const request = require("request");

const notImplemented = (method) => {
  throw new errors.NotImplemented(
    `${method} is not implemented on this service`
  );
};

class Service {
  constructor(options) {
    this.options = options || {};
  }

  async create(dat, params) {
    const sequelize = this.options.sequelize;
    const context = this;

    //   console.log(data);
    //console.log(params);

    const { payload } = params.authentication;



    let createdBy = payload.id;
    let data = dat[0].all;
   

    console.log(createdBy);

   
    if (
      !data.name ||
          !data.gender ||
          !data.age
    ) throw new BadRequest('Please provide all the information needed to create a character');

    await params.transaction.then((t) => {
      
      return sequelize.models.characters.create({
        raw: true,
        name: data.name,
        gender: data.gender,
        culture: data.culture,
        age: data.age,
        played_by: data.played_by,
        created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
        created_by: createdBy
      }, {
        transaction: t
      });
    });


    return Promise.resolve({
      result: "Character record has been added successfully!",
      character: data.name,
    });
  }

  async get() {
    notImplemented("get");
  }

  async find() {
    notImplemented("find");
  }

  async update() {
    notImplemented("update");
  }

  async patch() {
    notImplemented("patch");
  }

  async remove() {
    notImplemented("remove");
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
