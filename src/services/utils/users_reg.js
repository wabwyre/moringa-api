/* eslint-disable semi */
/* eslint-disable indent */
/* eslint-disable no-async-promise-executor */
/* eslint-disable no-unused-vars */

const {
  NotFound,
  BadRequest
} = require('@feathersjs/errors');
  
const errors = require('@feathersjs/errors');
const moment = require('moment');
const bcrypt = require('bcrypt');
const uuid = require('uuid').v4;
const makePassword = (pw) => {
    return new Promise(async rs => {
      let salt, hash;
      salt = await bcrypt.genSalt(10);
      hash = await bcrypt.hash(pw, salt);
      return rs(hash);
    })
  }

const notImplemented = method => {
  throw new errors.NotImplemented(`${method} is not implemented on this service`);
};
  
  
class Service {
  
  constructor(options) {
    this.options = options || {};
  }
  
  // setup(app) {
  //   this.app = app;
  // }
  
  async create(dat, params) {
  
    const sequelize = this.options.sequelize;
    const context = this;
  
    let data = dat[0].all;
    let randomstring = Math.random().toString(36).slice(-8);
    let password = await makePassword(randomstring);
  
    //
    console.log(data);
    //console.log(params);
  
    const {
      payload,
      user,
    } = params.authentication;
  
    let hashed_password = null;
    let user_id = uuid();
    let createdBy = payload.id;
    let group_id = '1';
   
  
    if (
      !data.firstname ||
        !data.lastname ||
        !data.email
    ) throw new BadRequest('Please provide all the information needed to create an account');
  
   

    await params.transaction.then(t => {
  
      return sequelize.models.users.create({
        raw: true,
        user_id: user_id,
        firstname: data.firstname,
        lastname: data.lastname,
        email: data.email,
        group_id: group_id,
        created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
        passport_path: typeof (dat[0].passportPath) !== 'undefined' ? dat[0].passportPath : '',
        password: password,
        created_by: createdBy
      }, {
        transaction: t
      });
  
    });
  
    return Promise.resolve({
      result: 'User created successfully',
      user_id: user_id,
    });
  
  }
  
  async get() {
    notImplemented('get');
  }
  
  async find() {
    notImplemented('find');
  }
  
  async update() {
    notImplemented('update');
  }
  
  async patch() {
    notImplemented('patch');
  }
  
  async remove() {
    notImplemented('remove');
  }
}
  
module.exports = function (options) {
  return new Service(options);
};
  
module.exports.Service = Service;