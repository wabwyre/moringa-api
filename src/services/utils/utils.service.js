/* eslint-disable indent */
/* eslint-disable no-unused-vars */
// Initializes the `utils` service on path `/utils`
const { Utils } = require('./utils.class');
const hooks = require('./utils.hooks');
const {
  authenticate
} = require('@feathersjs/authentication').hooks;
const checkPermissions = require('feathers-permissions');
const { BadRequest } = require('@feathersjs/errors');

const {
  disallow
} = require('feathers-hooks-common');

const multer = require('multer');
const authHooks = require('./auth.hooks');
const createHooks = require('./create.hooks');
const authService = require('./auth');
const adminReg = require('./users_reg');
const tokenCheck = require('./check');
const createBook = require('./create_books');
const createCharacter = require('./create_characters');
const createComment = require('./create_comments');

const storage = multer.diskStorage({
  destination: (_req, _file, cb) => cb(null, 'public'), // where the files are being stored
  filename: (_req, file, cb) => cb(null, `${Date.now()}-${file.originalname}`) // getting the file name
});

const upload = multer({
  storage,
  limits: {
    fieldSize: 100000, // Max field value size
    fileSize: 6 * 1000 * 1000, //Max file size `bytes`,
    files: 1, //Max no. of files for parallel uploading
  }
});


module.exports = function (app) {
  const sequelize = app.get('sequelizeClient');
  const paginate = app.get('paginate');
  const options = {
    sequelize,
    paginate: {
      default: 10,
      max: 100000
    }
  };

  app.use('/utils/auth', authService());
  app.use('/utils/check', tokenCheck());
  app.use('/utils/create-book', createBook(options));
  app.use('/utils/create-character', createCharacter(options));
  app.use('/utils/create-comment', createComment(options));
  
  
  app.use('/users/registration',
    upload.fields(
      [{
        name: 'passport',
        maxCount: 1
      },
      ]), (req, _res, next) => {
        const {
          method
        } = req;
        if (method === 'POST') {

          req.feathers.files = req.files; // transfer the received files to feathers        

          // for transforming the request to the model shape
          const body = [];
          //for (const file of req.files)
          body.push({
            all: req.body,
            passportName: typeof (req.files.passport) !== 'undefined' ? req.files.passport[0].originalname : '',
            passportPath: typeof (req.files.passport) !== 'undefined' ? req.files.passport[0].path : '',
          });

          req.body = method === 'POST' ? body : body[0];
        }
        next();
  }, adminReg(options));

  app.service('/users/registration').hooks({
    before: {
      all: [authenticate('jwt')],
      find: [disallow()],
      get: [disallow()],
      create: [],
      update: [disallow()],
      patch: [disallow()],
      remove: [disallow()]
    }
  });

  app.service('/utils/create-book').hooks({
    before: {
      all: [authenticate('jwt')],
      find: [disallow()],
      get: [disallow()],
      create: [],
      update: [disallow()],
      patch: [disallow()],
      remove: [disallow()]
    }
  });

  app.service('/utils/create-character').hooks({
    before: {
      all: [authenticate('jwt')],
      find: [disallow()],
      get: [disallow()],
      create: [],
      update: [disallow()],
      patch: [disallow()],
      remove: [disallow()]
    }
  });

  app.service('/utils/create-comment').hooks({
    before: {
      all: [authenticate('jwt')],
      find: [disallow()],
      get: [disallow()],
      create: [],
      update: [disallow()],
      patch: [disallow()],
      remove: [disallow()]
    }
  });
  
};
