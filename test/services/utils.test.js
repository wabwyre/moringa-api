const app = require('../../src/app');

describe('\'utils\' service', () => {
  it('registered the service', () => {
    const service = app.service('utils');
    expect(service).toBeTruthy();
  });
});
